package hrytsenko.example;

import java.util.Map;

public final class Maps {

    private Maps() {
    }

    /**
     * Represents the list of values as the map.
     * 
     * <p>
     * Assumes that values are listed in key-value order: K1, V1, K2, V2 and so on.
     * 
     * @param values
     *            the list of values.
     * 
     * @return the map with values.
     * 
     * @param <K>
     *            the type of keys.
     * @param <V>
     *            the type of values.
     */
    public static <K, V> Map<K, V> asMap(Object... values) {
        throw new UnsupportedOperationException();
    }

}
