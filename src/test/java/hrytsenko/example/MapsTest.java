package hrytsenko.example;

import java.util.Collections;
import java.util.Map;

import org.junit.Assert;
import org.junit.Test;

import com.google.common.collect.ImmutableMap;

public class MapsTest {

    @Test
    public void asMap_noEntries() {
        Map<?, ?> map = Maps.asMap();

        Assert.assertEquals(Collections.emptyMap(), map);
    }

    @Test
    public void asMap_oneEntry() {
        Map<String, String> map = Maps.asMap("K1", "V1");

        Assert.assertEquals(ImmutableMap.of("K1", "V1"), map);
    }

    @Test
    public void asMap_severalEntries() {
        Map<String, String> map = Maps.asMap("K1", "V1", "K2", "V2");

        Assert.assertEquals(ImmutableMap.of("K1", "V1", "K2", "V2"), map);
    }

    @Test(expected = IllegalArgumentException.class)
    public void asMap_incorrectEntry() {
        Maps.asMap("K1");
    }

}
