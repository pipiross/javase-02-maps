# Tasks
* (_elementary_) Provide an implementation that passes all tests.
* (_advanced_) Provide an implementation using the Streams API (control statements are not allowed).
